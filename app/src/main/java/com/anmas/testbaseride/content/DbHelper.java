package com.anmas.testbaseride.content;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Andrei on 06.10.2015.
 */
class DbHelper extends SQLiteOpenHelper {

    static final String TABLE_SHOPS = "shops";
    static final String NAME = "name";
    static final String DISTANCE = "distance";

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "TestDB";

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_SHOPS + " ("
                + BaseColumns._ID + " integer primary key autoincrement, "
                + NAME + " text, "
                + DISTANCE + " integer"
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
