package com.anmas.testbaseride.content;

/**
 * Created by Andrei on 06.10.2015.
 */
public interface DbResponse<T> {
    void onResult(T data);

    void onError(Exception e);

}
