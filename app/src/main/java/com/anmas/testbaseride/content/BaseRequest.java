package com.anmas.testbaseride.content;


import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;

/**
 * Created by Andrei on 06.10.2015.
 */
abstract class BaseRequest<T> implements Runnable {

    private final DbResponse<T> listener;
    private final Handler handler;

    public BaseRequest(DbResponse<T> listener) {
        this.listener = listener;
        this.handler = new Handler();
    }

    @Override
    public final void run() {
        SQLiteDatabase db = null;
        try {
            db = DAO.getInstance().getDbHelper().getWritableDatabase();
            final T result = executeInBackground(db);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    listener.onResult(result);
                }
            });
        } catch (final Exception e) {
            e.printStackTrace();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    listener.onError(e);
                }
            });
        } finally {
            if (db != null && db.isOpen())
                db.close();
        }
    }

    protected abstract T executeInBackground(SQLiteDatabase db);

}
