package com.anmas.testbaseride.content.parsers;

import com.anmas.testbaseride.content.models.Shop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrei on 06.10.2015.
 */
public class JsonHelper {

    public static String toJson(List<Shop> shops) {
        JSONArray array = new JSONArray();
        for (Shop shop : shops) {
            JSONObject object = new JSONObject();
            try {
                object.put(Shop.NAME, shop.getName());
                object.put(Shop.DISTANCE, shop.getDistance());
                array.put(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return array.toString();
    }

    public static List<Shop> parseJson(String json) {
        List<Shop> result = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(json);
            for (int i = 0; i < array.length(); i++) {

                JSONObject object = (JSONObject) array.get(i);
                String name = object.getString(Shop.NAME);
                int distance = object.getInt(Shop.DISTANCE);
                Shop model = new Shop(name, distance);
                result.add(model);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

}
