package com.anmas.testbaseride.content.models;

/**
 * Created by Andrei on 06.10.2015.
 */
public class Shop {

    public final static String NAME = "name";
    public final static String DISTANCE = "distance";
    private final String name;
    private final int distance;
    private long id;

    public Shop(String name, int distance) {
        this.name = name;
        this.distance = distance;
    }

    public Shop(long id, String name, int distance) {
        this.id = id;
        this.name = name;
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public int getDistance() {
        return distance;
    }

    public long getId() {
        return id;
    }
}
