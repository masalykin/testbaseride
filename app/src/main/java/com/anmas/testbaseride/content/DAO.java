package com.anmas.testbaseride.content;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.anmas.testbaseride.content.models.Shop;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Andrei on 06.10.2015.
 */
public class DAO {

    private static final int THREADS_COUNT = 1;

    private static DAO instance;
    private final SQLiteOpenHelper dbHelper;
    private final ExecutorService executor;

    private DAO(Context context) {
        dbHelper = new DbHelper(context);
        executor = Executors.newFixedThreadPool(THREADS_COUNT);
    }

    public static void createInstance(Application appContext) {
        if (instance == null)
            instance = new DAO(appContext);

    }

    public static DAO getInstance() {
        return instance;
    }

    SQLiteOpenHelper getDbHelper() {
        return dbHelper;
    }

    public void getShops(DbResponse<List<Shop>> listener) {
        BaseRequest<List<Shop>> request = new BaseRequest<List<Shop>>(listener) {
            @Override
            protected List<Shop> executeInBackground(SQLiteDatabase db) {
                List<Shop> result = new ArrayList<>();
                Cursor cursor = db.query(DbHelper.TABLE_SHOPS, null, null, null, null, null, null);
                if (cursor != null) {
                    int position = 0;
                    while (cursor.moveToPosition(position) && !cursor.isAfterLast()) {
                        long id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
                        String name = cursor.getString(cursor.getColumnIndex(DbHelper.NAME));
                        int distance = cursor.getInt(cursor.getColumnIndex(DbHelper.DISTANCE));
                        Shop shop = new Shop(id, name, distance);
                        result.add(shop);
                        position++;
                    }
                    cursor.close();
                }
                return result;
            }
        };
        executor.execute(request);
    }

    public void getShop(final long id, DbResponse<Shop> listener) {
        BaseRequest<Shop> request = new BaseRequest<Shop>(listener) {
            @Override
            protected Shop executeInBackground(SQLiteDatabase db) {
                Shop result = null;
                Cursor cursor = db.query(DbHelper.TABLE_SHOPS, null, BaseColumns._ID + " = ? ", new String[]{String.valueOf(id)}, null, null, null);
                if (cursor != null && cursor.moveToPosition(0)) {
                    String name = cursor.getString(cursor.getColumnIndex(DbHelper.NAME));
                    int distance = cursor.getInt(cursor.getColumnIndex(DbHelper.DISTANCE));
                    result = new Shop(name, distance);
                    cursor.close();
                }
                return result;
            }
        };
        executor.execute(request);
    }

    public void saveShops(final List<Shop> shops, DbResponse<Integer> listener) {
        BaseRequest<Integer> request = new BaseRequest<Integer>(listener) {

            @Override
            protected Integer executeInBackground(SQLiteDatabase db) {
                int count = 0;
                for (Shop shop : shops) {
                    ContentValues cv = new ContentValues();
                    cv.put(DbHelper.NAME, shop.getName());
                    cv.put(DbHelper.DISTANCE, shop.getDistance());
                    db.insert(DbHelper.TABLE_SHOPS, null, cv);
                }
                count += shops.size();
                return count;
            }
        };
        executor.execute(request);
    }

    public void clearShops(DbResponse<Integer> listener) {
        BaseRequest<Integer> request = new BaseRequest<Integer>(listener) {

            @Override
            protected Integer executeInBackground(SQLiteDatabase db) {
                return db.delete(DbHelper.TABLE_SHOPS, null, null);
            }
        };
        executor.execute(request);
    }


}
