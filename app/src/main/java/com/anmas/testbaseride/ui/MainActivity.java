package com.anmas.testbaseride.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.anmas.testbaseride.R;
import com.anmas.testbaseride.content.models.Shop;
import com.anmas.testbaseride.ui.fragments.ItemsListFragment;

public class MainActivity extends AppCompatActivity implements ItemsListFragment.ShowShopDetailsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content, new ItemsListFragment())
                    .commit();
        }

    }

    @Override
    public void showShopDetails(Shop item) {
        Intent details = new Intent(this, DetailsActivity.class);
        details.putExtra(DetailsActivity.EXTRA_ID, item.getId());
        startActivity(details);
    }

}
