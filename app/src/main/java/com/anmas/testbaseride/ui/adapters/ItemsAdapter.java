package com.anmas.testbaseride.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.anmas.testbaseride.R;
import com.anmas.testbaseride.content.models.Shop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrei on 06.10.2015.
 */
public class ItemsAdapter extends BaseAdapter {

    private final Context context;
    private List<Shop> items = new ArrayList<>();

    public ItemsAdapter(Context context) {
        this.context = context;
    }

    public void setItems(List<Shop> items) {
        if (items != null)
            this.items = items;
        else
            this.items.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.v_shop_list_item, parent, false);
            ViewHolder vh = new ViewHolder();
            vh.textName = (TextView) convertView.findViewById(R.id.shop_name);
            vh.textDistance = (TextView) convertView.findViewById(R.id.shop_distance);
            convertView.setTag(vh);
        }
        ViewHolder vh = (ViewHolder) convertView.getTag();
        Shop shop = (Shop) getItem(position);
        vh.textName.setText(shop.getName());
        vh.textDistance.setText(String.format(context.getString(R.string.km), shop.getDistance() / 1000.f));
        return convertView;
    }

    private static class ViewHolder {
        TextView textName;
        TextView textDistance;
    }

}
