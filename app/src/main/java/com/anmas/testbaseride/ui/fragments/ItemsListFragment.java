package com.anmas.testbaseride.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.anmas.testbaseride.R;
import com.anmas.testbaseride.content.DAO;
import com.anmas.testbaseride.content.DbResponse;
import com.anmas.testbaseride.content.models.Shop;
import com.anmas.testbaseride.content.parsers.JsonHelper;
import com.anmas.testbaseride.ui.adapters.ItemsAdapter;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrei on 06.10.2015.
 */
public class ItemsListFragment extends Fragment {

    private static final List<Shop> shops = Arrays.asList(
            new Shop("Лиловый", 15200),
            new Shop("Крем", 28000),
            new Shop("Шарлотка", 123100),
            new Shop("Полироль", 76500),
            new Shop("Плед", 0),
            new Shop("Каркас", 46900),
            new Shop("Меконг", 32400),
            new Shop("Гарда", 0),
            new Shop("Наушник", 0),
            new Shop("Древо", 0)
    );

    private ItemsAdapter adapter;
    private ShowShopDetailsListener showDetailsListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            showDetailsListener = (ShowShopDetailsListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Use this fragment with ShowShopDetailsListener activity");
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_list, container, false);
        initListView(view);
        initViews(view);
        return view;
    }

    private void initListView(View view) {
        ListView listView = (ListView) view.findViewById(android.R.id.list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (showDetailsListener != null) {
                    showDetailsListener.showShopDetails((Shop) parent.getItemAtPosition(position));
                }
            }
        });
        adapter = new ItemsAdapter(getActivity());
        listView.setAdapter(adapter);
    }

    private void initViews(View view) {
        Button clearDb = (Button) view.findViewById(R.id.btnClearDbs);
        clearDb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearDb();
            }
        });
        Button fillDbWithData = (Button) view.findViewById(R.id.btnFillDbWithData);
        fillDbWithData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFillDbWithData();
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    private void loadData() {
        DAO.getInstance().getShops(new DbResponse<List<Shop>>() {
            @Override
            public void onResult(List<Shop> data) {
                adapter.setItems(data);
            }

            @Override
            public void onError(Exception e) {
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clearDb() {
        DAO.getInstance().clearShops(new DbResponse<Integer>() {
            @Override
            public void onResult(Integer data) {
                loadData();
                Toast.makeText(getActivity(), String.format(getString(R.string.deleted_mes), data), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Exception e) {
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setFillDbWithData() {
        String serialized = JsonHelper.toJson(shops);
        Toast.makeText(getActivity(), serialized, Toast.LENGTH_SHORT).show();
        DAO.getInstance().saveShops(JsonHelper.parseJson(serialized), new DbResponse<Integer>() {
            @Override
            public void onResult(Integer data) {
                loadData();
                Toast.makeText(getActivity(), String.format(getString(R.string.added_mes), data), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Exception e) {
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public interface ShowShopDetailsListener {
        void showShopDetails(Shop shop);
    }
}
