package com.anmas.testbaseride.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anmas.testbaseride.R;
import com.anmas.testbaseride.content.models.Shop;

/**
 * Created by Andrei on 06.10.2015.
 */
public class DetailFragment extends Fragment {

    private TextView shopName;
    private TextView shopDistance;
    private Shop itemToShow;

    public void setItemToShow(Shop itemToShow) {
        this.itemToShow = itemToShow;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_details, container, false);
        initViews(view);
        showData();
        return view;
    }

    private void initViews(View view) {
        shopName = (TextView) view.findViewById(R.id.shop_name);
        shopDistance = (TextView) view.findViewById(R.id.shop_distance);
    }

    private void showData() {
        if (itemToShow != null) {
            shopName.setText(itemToShow.getName());
            shopDistance.setText(String.format(getString(R.string.m), itemToShow.getDistance()));
        }
    }

}
