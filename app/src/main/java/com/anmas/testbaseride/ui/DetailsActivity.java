package com.anmas.testbaseride.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.anmas.testbaseride.R;
import com.anmas.testbaseride.content.DAO;
import com.anmas.testbaseride.content.DbResponse;
import com.anmas.testbaseride.content.models.Shop;
import com.anmas.testbaseride.ui.fragments.DetailFragment;

/**
 * Created by Andrei on 06.10.2015.
 */
public class DetailsActivity extends AppCompatActivity {

    public static final String EXTRA_ID = "extra_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        long id = getIntent().getExtras().getLong(EXTRA_ID);
        DAO.getInstance().getShop(id, new DbResponse<Shop>() {
            @Override
            public void onResult(Shop data) {
                DetailFragment fragment = new DetailFragment();
                fragment.setItemToShow(data);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content, fragment, "details")
                        .commit();
            }

            @Override
            public void onError(Exception e) {

            }
        });
    }
}
