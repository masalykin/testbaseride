package com.anmas.testbaseride;

import android.app.Application;

import com.anmas.testbaseride.content.DAO;

/**
 * Created by Andrei on 06.10.2015.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DAO.createInstance(this);
    }
}
